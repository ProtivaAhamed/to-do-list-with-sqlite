package com.example.todolist.MainClass;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.todolist.Adapter.AdapterTaskTracker;
import com.example.todolist.Fragment.DatePickerFragment;
import com.example.todolist.PojoClass.TaskTracker;
import com.example.todolist.R;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    //Data Handleing arrayLists
    String User_name;
    TextView text;
    ArrayList<String> String_Data_Of_Task_Tracker = new ArrayList<>();
    private ArrayList<TaskTracker> Cards = new ArrayList<>();
    ArrayList<TaskTracker> TaskTrackerData = new ArrayList<>();
    // end of data handleing arrayList
    String day_of_month;
    String d1;
    TextView textView;
    FloatingActionButton spk;
    int c = -1;
    FirebaseDatabase firebaseDatabase;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AdapterTaskTracker adapterTaskTracker;
    int time = 2000;
    long backpress;
    //SpotsDialog spotsDialog = new SpotsDialog(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = findViewById(R.id.name);
        recyclerView = findViewById(R.id.recyclerView1);
        layoutManager = new LinearLayoutManager(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        textView = findViewById(R.id.date);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        User_name = getIntent().getStringExtra("UserName");
        text.setText(User_name);
        Toast.makeText(MainActivity.this, User_name, Toast.LENGTH_SHORT).show();
        Cards.clear();
        String_Data_Of_Task_Tracker.clear();
        spk = findViewById(R.id.fab1);
        spk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate(v);
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (backpress + time > System.currentTimeMillis()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(MainActivity.this, "Press again to exit", Toast.LENGTH_SHORT).show();
        }
        backpress = System.currentTimeMillis();
    }

    public void setDate(View view) {
        DialogFragment datePicker = new DatePickerFragment();
        datePicker.show(getSupportFragmentManager(), "date picker");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) { // get the date using date picker
        //  d = dayOfMonth + "/" + (month + 1) + "/" + year; // this one
        d1 = dayOfMonth + "-" + (month + 1) + "-" + year;
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        day_of_month = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
        Toast.makeText(MainActivity.this,"Time Picked: "+day_of_month,Toast.LENGTH_SHORT).show();


    }

    private void rearrangeDate(String day_of_month, String d) {
        String year, weekDay, DayWithMonth;
        String[] s = day_of_month.split(",");
        DayWithMonth = s[1];
        weekDay = s[0];
        year = s[2];
        TaskTracker t = new TaskTracker(DayWithMonth, year, weekDay, d, "0", User_name);

    }



    private void addToCart() // only unique date will be added
    {
        Cards.clear();
        if (String_Data_Of_Task_Tracker.size() == 1) {
            String[] temp = String_Data_Of_Task_Tracker.get(0).split(",");
            TaskTracker taskTracker = new TaskTracker(temp[1], temp[3], temp[2], temp[0], temp[4], temp[5]);
            if (temp[5].equals(User_name)) {
                Cards.add(taskTracker);
            }
            Log.d("TaskTrackerData", String_Data_Of_Task_Tracker.get(0));
            c++;
        } else {
            for (int i = c + 1; i < String_Data_Of_Task_Tracker.size(); i++) {
                String[] temp = String_Data_Of_Task_Tracker.get(i).split(",");
                TaskTracker taskTracker = new TaskTracker(temp[1], temp[3], temp[2], temp[0], temp[4], temp[5]);
                if (temp[5].equals(User_name)) {
                    Cards.add(taskTracker);

                }
                Log.d("TaskTrackerData", String_Data_Of_Task_Tracker.get(i));
                c++;
            }
        }


    }

    public void logout(View view) {
        Intent intent = new Intent(MainActivity.this, LoginPage.class);
        startActivity(intent);
    }
}
