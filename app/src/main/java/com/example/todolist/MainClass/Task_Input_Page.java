package com.example.todolist.MainClass;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.todolist.Fragment.TimePickerFragment;
import com.example.todolist.PojoClass.Tasks;
import com.example.todolist.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Task_Input_Page extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {
    String User_name="protiva";
    EditText time_picker;
    ImageView c;
    FloatingActionButton add;
    TextView textHeading;
    TextInputEditText title, description;
    FirebaseDatabase firebaseDatabase;
    String Title,Descripton,Time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_input_page);
        firebaseDatabase = FirebaseDatabase.getInstance();
        c = findViewById(R.id.Clock);
        textHeading=findViewById(R.id.day);
        title=findViewById(R.id.EnterTittle);
        description=findViewById(R.id.EnterDescription);
        time_picker = findViewById(R.id.EnterTime);
        add=findViewById(R.id.Add);
        // receiving values from the TaskByList class
        final String d = getIntent().getStringExtra("d");
        final String y = getIntent().getStringExtra("y");
        final String w = getIntent().getStringExtra("w");
        final String date = getIntent().getStringExtra("date");
        User_name=getIntent().getStringExtra("UserName");
        //Receiving values from the EditView

        String s = "Date:" + d + "," + y + " (" + w + ")";
        textHeading.setText(s);
        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTime();
            }
        }); // timePicker
        //getting values from the edittexts
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Title=title.getText().toString();
                Descripton=description.getText().toString();
                Time=time_picker.getText().toString();
                if(Title.equals("")|| Descripton.equals("")||Time.equals(""))
                {
                    Toast.makeText(Task_Input_Page.this,"Field Remained Empty",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Tasks task=new Tasks(Title,Descripton,Time,date,User_name);
                    SaveDataTask(task);
                    title.getText().clear();
                    description.getText().clear();
                    time_picker.getText().clear();
                    Intent intent=new Intent(Task_Input_Page.this, TaskListByDate.class);
                    intent.putExtra("date", task.getDate());
                    intent.putExtra("DayAndMonth",d);
                    intent.putExtra("week",w);
                    intent.putExtra("year",y);
                    intent.putExtra("UserName",User_name);
                    startActivity(intent);
                }
            }
        });

    }


    //firebase part
    private void SaveDataTask(Tasks task) {
        DatabaseReference databaseReference = firebaseDatabase.getReference("Tasks");
        String keyNode = databaseReference.push().getKey();
        task.setKey(keyNode);
        databaseReference.child(keyNode).setValue(task);
        Toast.makeText(Task_Input_Page.this, "Data Saved for task", Toast.LENGTH_LONG).show();
    }

    // for time picker
    public void setTime() {
        DialogFragment timePicker = new TimePickerFragment();
        timePicker.show(getSupportFragmentManager(), "time picker");
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        int hour = hourOfDay;
        int minutes = minute;
        String timeSet = "";
        if (hour > 12) {
            hour -= 12;
            timeSet = "PM";
        } else if (hour == 0) {
            hour += 12;
            timeSet = "AM";
        } else if (hour == 12) {
            timeSet = "PM";
        } else {
            timeSet = "AM";
        }
        String min = "";
        if (minutes < 10)
            min = "0" + minutes;
        else
            min = String.valueOf(minutes);

        String aTime = new StringBuilder().append(hour).append(':')
                .append(min).append(" ").append(timeSet).toString();
        time_picker.setText(aTime);
    }
}

