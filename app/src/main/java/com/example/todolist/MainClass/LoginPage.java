package com.example.todolist.MainClass;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.todolist.MyDatabaseHandler;
import com.example.todolist.PojoClass.Name;
import com.example.todolist.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class LoginPage extends AppCompatActivity {
    MyDatabaseHandler myDatabaseHandler;
    int UserExsists=0;
    int time = 2000;
    long backpress;
    TextInputEditText textInputEditText;
    Button sign_in, sign_up;
    String UserName;
    int SignIn = 0;
    int SignUp = 0;
    String Status = "new";
    FirebaseDatabase firebaseDatabase;
    ArrayList<Name>users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        myDatabaseHandler=new MyDatabaseHandler(this);
        SQLiteDatabase db= myDatabaseHandler.getWritableDatabase();
        textInputEditText = findViewById(R.id.En);
        firebaseDatabase = FirebaseDatabase.getInstance();
        Check_Local_Database();
        sign_in = findViewById(R.id.SignIn);
        sign_up = findViewById(R.id.SignUp);
        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserName = textInputEditText.getText().toString();
                SignIn = 1;
                if (UserName.equals("")) {
                    Toast.makeText(LoginPage.this, "Enter a name", Toast.LENGTH_SHORT).show();
                    SignIn = 0;
                } else {
                  //  Toast.makeText(LoginPage.this, "SignIn", Toast.LENGTH_SHORT).show();
                    SearchUser(UserName);
                    if(UserExsists==1)
                    {
                        Intent intent=new Intent(LoginPage.this,MainActivity.class);
                        intent.putExtra("UserName",UserName);
                        startActivity(intent);
                    }
                }

            }
        });

        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserName = textInputEditText.getText().toString();
                SignUp = 1;
                if (UserName.equals("")) {
                    Toast.makeText(LoginPage.this, "Enter a name", Toast.LENGTH_SHORT).show();
                    SignUp = 0;
                } else {
              //      Toast.makeText(LoginPage.this, "SignUp", Toast.LENGTH_SHORT).show();
                    SearchUser(UserName);
                    if(UserExsists==0)
                    {
                        long rowId=  myDatabaseHandler.Insert_User_In_Login_Table(UserName);
                        if (rowId != -1) {
                             Toast.makeText(LoginPage.this,"Successfully inserted",Toast.LENGTH_LONG).show();
                             textInputEditText.setText("");
                        }
                    }
                }
            }
        });
    }

    private void Check_Local_Database() {
        Cursor cursor=myDatabaseHandler.get_all_registered_user_data();
        if(cursor.getCount()==0)
        {
            //check firebase and get the data from there and upload it to the local database
        }
    }

    private void SearchUser(String user) {
      Cursor cursor=myDatabaseHandler.Check_User(user);
        if (cursor.getCount() == 0) {
            Toast.makeText(LoginPage.this, "User doesn't exits", Toast.LENGTH_SHORT).show();
            UserExsists=0;
        }
        else{
            Toast.makeText(LoginPage.this, "User  exits", Toast.LENGTH_SHORT).show();
            UserExsists=1;
        }
    }


    public void onBackPressed() {
        if (backpress + time > System.currentTimeMillis()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(LoginPage.this, "Press again to exit", Toast.LENGTH_SHORT).show();
        }
        backpress = System.currentTimeMillis();
    }


}
