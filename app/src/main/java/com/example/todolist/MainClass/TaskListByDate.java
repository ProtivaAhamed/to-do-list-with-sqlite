package com.example.todolist.MainClass;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.todolist.Adapter.AdapterTaskList;
import com.example.todolist.PojoClass.TaskTracker;
import com.example.todolist.PojoClass.Tasks;
import com.example.todolist.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.varunest.sparkbutton.SparkButton;

import java.util.ArrayList;

public class TaskListByDate extends AppCompatActivity {
    String User_name="protiva";
    TextView day, year, w;
    SparkButton Back;
    FirebaseDatabase firebaseDatabase;
    FloatingActionButton f;
    String date;
    int count = 0;
    ArrayList<Tasks> tasks = new ArrayList<>();
    ArrayList<String> dataFromServer = new ArrayList<>();
    ArrayList<TaskTracker> TaskTrackerData = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AdapterTaskList adapterTaskList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_list_day_date);
        firebaseDatabase = FirebaseDatabase.getInstance();
        recyclerView = findViewById(R.id.recyclerView2);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        day = findViewById(R.id.dateyear);
        year = findViewById(R.id.year);
        w = findViewById(R.id.week);
        f = findViewById(R.id.fab);
        date = getIntent().getStringExtra("date");
        final String month = getIntent().getStringExtra("DayAndMonth");
        final String year = getIntent().getStringExtra("year");
        final String week = getIntent().getStringExtra("week");
        User_name=getIntent().getStringExtra("UserName");
        day.setText(month + "," + year);
        w.setText(week);
        getData();
    //    Back = findViewById(R.id.backPress);

        f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TaskListByDate.this, Task_Input_Page.class);
                intent.putExtra("y", year);
                intent.putExtra("w", week);
                intent.putExtra("d", month);
                intent.putExtra("date", date);
                intent.putExtra("UserName",User_name);
                startActivity(intent);
            }
        });
    }
    //Rearranging all the datas

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent= new Intent(TaskListByDate.this, MainActivity.class);
        intent.putExtra("UserName",User_name);
        startActivity(intent);
    }

    private void addToCard() {
        for (String s : dataFromServer) {
            String[] str = s.split(",");
            Tasks t = new Tasks(str[0], str[1], str[2], str[3],str[5]);
               if(str[5].equals(User_name)) {
                   tasks.add(t);
               }
            count++;
        }
    }

    public void loadData() {
        adapterTaskList = new AdapterTaskList(this, tasks);
        recyclerView.setAdapter(adapterTaskList);
    }

    //firebase
    public void getData() {
        tasks.clear();
        count = 0;
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Tasks");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.getChildrenCount() == 0) {
                    Toast.makeText(TaskListByDate.this, "DataBase Empty", Toast.LENGTH_SHORT).show();
                } else {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        Tasks task = dataSnapshot1.getValue(Tasks.class);
                        if (task.getDate().equals(date)) {
                            dataFromServer.add(task.getTittle() + "," + task.getDescription() + "," + task.getTime() + "," + task.getDate() + "," + task.getKey()+","+task.getid());
                            count++;
                        }
                    }
                    addToCard();
                    loadData();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}