package com.example.todolist.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.todolist.R;
import com.example.todolist.MainClass.TaskListByDate;
import com.example.todolist.PojoClass.TaskTracker;

import java.util.ArrayList;

public class AdapterTaskTracker extends RecyclerView.Adapter<AdapterTaskTracker.viewHolder> {
    private Context context;
    private ArrayList<TaskTracker>cards;

    public AdapterTaskTracker(Context context, ArrayList<TaskTracker> cards) {
        this.context = context;
        this.cards = cards;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
     View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.task_tracker_item,viewGroup,false);
     viewHolder v= new viewHolder(view);
        return v;
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder v, int i) {
        final TaskTracker task= cards.get(i);
    //    Toast.makeText(context,"OnBinding",Toast.LENGTH_LONG).show();
        v.weekDay.setText(task.getWeekDay());
        v.DaywithMonth.setText(task.getDayWithMonth());
        v.Year.setText(task.getYear());
       v.WorkNo.setText(task.getCount());
        v.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, TaskListByDate.class);
                intent.putExtra("date",task.getDate());
                intent.putExtra("DayAndMonth",task.getDayWithMonth());
                intent.putExtra("year",task.getYear());
                intent.putExtra("week",task.getWeekDay());
                intent.putExtra("UserName",task.getUserId());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return cards.size();
    }


    public class viewHolder extends RecyclerView.ViewHolder {
      public  TextView weekDay,DaywithMonth,Year,WorkNo;
       public CardView card;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            card=itemView.findViewById(R.id.cardview1);
            weekDay=itemView.findViewById(R.id.weekDay);
           DaywithMonth=itemView.findViewById(R.id.DayWithMonth);
            Year=itemView.findViewById(R.id.year);
            WorkNo=itemView.findViewById(R.id.taskNo);
        }
    }
}
