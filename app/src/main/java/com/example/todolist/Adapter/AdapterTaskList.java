package com.example.todolist.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.todolist.R;
import com.example.todolist.PojoClass.Tasks;

import java.util.ArrayList;

public class AdapterTaskList extends RecyclerView.Adapter<AdapterTaskList.viewHolder> {
    private Context context;
    private ArrayList<Tasks> tasks;

    public AdapterTaskList(Context context, ArrayList<Tasks> tasks) {
        this.context = context;
        this.tasks = tasks;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tasklist_item, viewGroup, false);
        viewHolder v = new viewHolder(view);
        return v;
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int i) {
        Tasks t=tasks.get(i);
        viewHolder.title.setText(t.getTittle());
        viewHolder.description.setText(t.getDescription());
        viewHolder.time.setText(t.getTime());

    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
 TextView title, description,time;
 CheckBox checkBox;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.tittle);
            time=itemView.findViewById(R.id.time);
            description=itemView.findViewById(R.id.description);
            checkBox=itemView.findViewById(R.id.checkbox);
        }
    }
}
