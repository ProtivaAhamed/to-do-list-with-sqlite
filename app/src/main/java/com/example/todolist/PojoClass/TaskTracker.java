package com.example.todolist.PojoClass;

import java.util.ArrayList;

public class TaskTracker {
    private String date;
    private String DayWithMonth;
    private String Year;
    private String WeekDay;
    private String count;
    private String UserId;
public TaskTracker(){

}
    public TaskTracker(String dayWithMonth, String year, String weekDay, String d,String count,String UserId) {
        DayWithMonth = dayWithMonth;
        Year = year;
        WeekDay = weekDay;
        this.count=count;
        date = d;
        this.UserId=UserId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDayWithMonth() {
        return DayWithMonth;
    }

    public void setDayWithMonth(String dayWithMonth) {
        DayWithMonth = dayWithMonth;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getWeekDay() {
        return WeekDay;
    }

    public void setWeekDay(String weekDay) {
        WeekDay = weekDay;
    }


}
