package com.example.todolist.PojoClass;

public class Name {
  private   String name;
    private String namekey;
    public Name()
    {

    }

    public Name(String name,String namekey) {
        this.name = name;
        this.namekey=namekey;
    }

    public String getNamekey() {
        return namekey;
    }

    public void setNamekey(String namekey) {
        this.namekey = namekey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
