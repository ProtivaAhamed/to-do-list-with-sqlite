package com.example.todolist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class MyDatabaseHandler extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "To-Do-List.db";
    private static final int VERSION_NUMBER = 1;
    private static  final String TABLE1="Registered_User";
    private static final String TABLE2="Date_List";
    private static final String TABLE3="Task_List";
    Context context;

    public MyDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, VERSION_NUMBER);
        this.context=context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Toast.makeText(context, "On Create Is Called ", Toast.LENGTH_SHORT).show();
            db.execSQL("CREATE TABLE Registered_User (User varchar(255));");
            Toast.makeText(context, "Registered_User in created", Toast.LENGTH_SHORT).show();
            db.execSQL("CREATE TABLE Date_List(id int NOT NULL PRIMARY KEY AUTOINCREMENT,UserName varchar(255) NOT NULL, date varchar(20) NOT NULL, DayWithMonth varchar(20) NOT NULL, Year varchar(20) NOT NULL, WeekDay varchar(20) NOT NULL,count varchar(20) NOT NULL ); ");
            Toast.makeText(context, "Date_List in created", Toast.LENGTH_SHORT).show();
            db.execSQL("CREATE TABLE Task_List(id int NOT NULL PRIMARY KEY AUTOINCREMENT, tittle varchar(255) NOT NULL,description varchar(500) NOT NULL,time varchar(20) NOT NULL,date varchar(20) NOT NULL,UserName varchar(20) NOT NULL );");
            Toast.makeText(context, "Task_List is Created", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(context, "Exception Caught: " + e, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            Toast.makeText(context, "OnUpgrade is called", Toast.LENGTH_SHORT).show();
            db.execSQL("DROP TABLE IF EXISTS "+TABLE1);
            db.execSQL("DROP TABLE IF EXISTS "+TABLE2);
            db.execSQL("DROP TABLE IF EXISTS "+TABLE3);
            onCreate(db);
        } catch (Exception e) {
            Toast.makeText(context, "Exception Caught: " + e, Toast.LENGTH_SHORT).show();
        }

    }


    public Cursor Check_User(String user) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM "+TABLE1+" WHERE User = ? ", new String[]{user});
        return cursor;
    }

    public long Insert_User_In_Login_Table(String userName) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("User",userName);
        long rowId = sqLiteDatabase.insert(TABLE1, null, contentValues);
        Toast.makeText(context, "" + userName, Toast.LENGTH_SHORT).show();
        return rowId;
    }

    public Cursor get_all_registered_user_data() {
       return null;
        }
    }
